package me.prog.projekt.window;



import me.prog.projekt.img.ImageManager;
import me.prog.projekt.img.JImage;
import me.prog.projekt.img.method.Method;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class Window implements ActionListener {
    private JFrame window;
    private JMenuBar menuBar;
    private JScrollPane scrollPane;
    private JPanel panel;
    private JLabel img;
    //Menus
    private JMenu file, edit;
    //Menu items
    private JMenuItem save;

    private JImage currentImage;

    public Window(){
        createWindow();
    }

    public void createWindow(){
        window = new JFrame();
        window.setTitle("Photoshop 2.0"); //Set window title
        window.setSize(480, 600);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLocationRelativeTo(null); //center
        //Default look
        JFrame.setDefaultLookAndFeelDecorated(true);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setPreferredSize(window.getSize());
        scrollPane = new JScrollPane(panel);
        window.setContentPane(scrollPane);

        //menu bar
        menuBar = new JMenuBar();
        //Menu
        file = new JMenu("File");
        edit = new JMenu("Edit");
        //File
        JMenuItem open = new JMenuItem("Open");
        save = new JMenuItem("Save");
        file.add(open);
        file.add(save);
        open.addActionListener(this);
        save.addActionListener(this);
        save.setEnabled(false);
        //Edit
        for(Method method : ImageManager.getManager().getMethods()){
            JMenuItem item = new JMenuItem(method.getName());
            item.addActionListener(this);
            edit.add(item);
        }
        //
        menuBar.add(file);
        menuBar.add(edit);
        window.setJMenuBar(menuBar);
        edit.setEnabled(false);
        window.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Open")){
            String path = promptForPath(false);
            if(path != null){
                currentImage = ImageManager.getManager().loadImage(path);
                displayImage();
                save.setEnabled(true);
                edit.setEnabled(true);
            }
        }
        else if(e.getActionCommand().equals("Save")){
            if(currentImage != null){
                String path = promptForPath(true);
                if(path != null){
                    if(currentImage.save(path)){
                        JOptionPane pane = new JOptionPane();
                        pane.createDialog("An error occured");
                    }
                }
            }
        }
        else{
            ImageManager.getManager().getMethod(e.getActionCommand()).run(currentImage);
            displayImage();
        }
    }

    public String promptForPath(boolean save){
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Images (.png, .jpg, .jpeg, .bmp)", "png", "bmp", "jpg", "jpeg");
        fileChooser.setFileFilter(filter);
        int retVal;
        if(save) retVal = fileChooser.showSaveDialog(window);
        else retVal = fileChooser.showOpenDialog(window);

        if(retVal == JFileChooser.APPROVE_OPTION){
            File file = fileChooser.getSelectedFile();
            return file.getAbsolutePath();
        }
        else {
            return null;
        }
    }

    public void displayImage(){
        if(img != null){
            panel.remove(img);
        }
        //Display image
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Rectangle bounds = env.getMaximumWindowBounds();
        int width = Math.min(currentImage.getImage().getWidth(), bounds.width);
        int height = Math.min(currentImage.getImage().getHeight(), bounds.height-50);
        ImageIcon imageIcon = new ImageIcon(currentImage.resize(width, height));
        img = new JLabel();
        img.setIcon(imageIcon);
        panel.add(img, BorderLayout.CENTER);

        //Set size
        panel.setPreferredSize(new Dimension(img.getIcon().getIconWidth(), img.getIcon().getIconHeight()));
        window.pack();
        width = Math.min(img.getIcon().getIconWidth()+20, bounds.width);
        height = Math.min(img.getIcon().getIconHeight()+65, bounds.height-50);
        window.setSize(new Dimension(width, height));
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
}
