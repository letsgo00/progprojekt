package me.prog.projekt.img;

public class JPG extends JImage {
    public JPG(String path) {
        super(path);
    }

    @Override
    public String getType() {
        return "jpg";
    }
}
