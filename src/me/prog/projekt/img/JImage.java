package me.prog.projekt.img;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public abstract class JImage {

    private BufferedImage image;

    public JImage(String path) {
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JImage(BufferedImage image) {
        this.image = image;
    }


    public abstract String getType();

    /**
     * Save image
     * @param path save location
     * @return true if save is successful, false if an error occurred
     */
    public boolean save(String path) {
        String type;
        if ((type = getType(path)) == null) {
            path += "." + getType();
            type = getType();
        }
        try {
            File file = new File(path);
            if(file.exists() && JOptionPane.showConfirmDialog(null, "Are you sure you want to overwrite " + path + "?", "", JOptionPane.YES_NO_OPTION) == 1){
                return true; // do not show error
            }
            ImageIO.write(image, type, file);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String getType(String path) {
        if (!path.contains(".")) return null;
        String[] strs = path.split("\\.");
        if(strs.length == 0) return null;
        String ext = strs[strs.length - 1];
        switch (ext) {
            case "png":
            case "jpg":
            case "jpeg":
            case "bmp":
                return ext;
        }
        return null;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    /**
     * Resize image
     * @param maxW Maximum width
     * @param maxH Maximum height
     * @return the resized image
     */
    public BufferedImage resize(int maxW, int maxH) {
        //Calculate maximum scaled size
        double widthRatio = ((double) maxW / image.getWidth());
        double heightRatio = ((double) maxH / image.getHeight());
        double ratio = Math.min(heightRatio, widthRatio);
        int w = (int) (ratio * image.getWidth());
        int h = (int) (ratio * image.getHeight());
        //Resize
        Image tmp = image.getScaledInstance(w, h, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }
}
