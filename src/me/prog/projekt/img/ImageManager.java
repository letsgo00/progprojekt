package me.prog.projekt.img;

import me.prog.projekt.img.method.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ImageManager {
    //Singleton
    private static ImageManager manager = new ImageManager();
    private List<Method> methods = new ArrayList<>();
    private ImageManager(){
        //Add all methods
        methods.add(new MirrorVertically());
        methods.add(new MirrorHorizontally());
        methods.add(new RotateCW());
        methods.add(new RotateCCW());
        methods.add(new GrayScale());
        methods.add(new Invert());
    }

    public static ImageManager getManager() {
        return manager;
    }

    public JImage loadImage(String path){
        String type = getType(path);
        switch (type){
            case "png":
                return new PNG(path);
            case "bmp":
                return new BMP(path);
            case "jpeg":
            case "jpg":
                return new JPG(path);
        }
        return null;
    }

    /**
     * Get the type of the file provided
     * @param path The file path
     * @return the file type
     */
    public String getType(String path){
        File input = new File(path);
        try {
            String type = Files.probeContentType(input.toPath());
            if(!type.contains("/") || type.split("/").length == 1) return type;
            return type.split("/")[1];
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Method> getMethods() {
        return methods;
    }

    public Method getMethod(String name){
        for (Method method : methods){
            if(method.getName().equals(name)) return method;
        }
        return null;
    }
}
