package me.prog.projekt.img;

public class BMP extends JImage {
    public BMP(String path) {
        super(path);
    }

    @Override
    public String getType() {
        return "bmp";
    }
}
