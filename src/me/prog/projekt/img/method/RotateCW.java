package me.prog.projekt.img.method;


import me.prog.projekt.img.JImage;

import java.awt.image.BufferedImage;

public class RotateCW implements Method {
    @Override
    public void run(JImage image) {
        int width = image.getImage().getWidth();
        int height = image.getImage().getHeight();
        BufferedImage newImage = new BufferedImage(height, width, image.getImage().getType());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                newImage.setRGB(height - y - 1, x, image.getImage().getRGB(x, y));
            }
        }
        image.setImage(newImage);
    }

    @Override
    public String getName() {
        return "Rotate CW";
    }
}
