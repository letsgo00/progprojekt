package me.prog.projekt.img.method;

import me.prog.projekt.img.JImage;

public interface Method {

    public void run(JImage image);

    public String getName();
}
