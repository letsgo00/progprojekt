package me.prog.projekt.img.method;

import me.prog.projekt.img.JImage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class GrayScale implements Method {
    @Override
    public void run(JImage image) {
        BufferedImage img = new BufferedImage(image.getImage().getWidth(), image.getImage().getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = img.getGraphics();
        g.drawImage(image.getImage(), 0, 0, null);
        g.dispose();
        image.setImage(img);
    }

    @Override
    public String getName() {
        return "Grayscale";
    }
}
