package me.prog.projekt.img.method;

import me.prog.projekt.img.JImage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Invert implements Method {
    @Override
    public void run(JImage image) {
        BufferedImage i = image.getImage();
        for (int x = 0; x < i.getWidth(); x++) {
            for (int y = 0; y < i.getHeight(); y++) {
                int rgba = i.getRGB(x, y);
                Color col = new Color(rgba, true);
                col = new Color(255 - col.getRed(),
                        255 - col.getGreen(),
                        255 - col.getBlue());
                i.setRGB(x, y, col.getRGB());
            }
        }
        image.setImage(i);
    }

    @Override
    public String getName() {
        return "Invert";
    }
}
