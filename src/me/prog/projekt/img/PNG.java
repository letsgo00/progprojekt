package me.prog.projekt.img;

public class PNG extends JImage {

    public PNG(String path) {
        super(path);
    }

    @Override
    public String getType() {
        return "png";
    }
}
